<h3 align="center">Jrnl-Generator</h3>

<div align="center">

  [![Status](https://img.shields.io/badge/status-active-success.svg)]() 

</div>

---

<p align="center">Python script that automates creating a journal files
    <br> 
</p>

## 📝 Table of Contents

- [About](#about)
- [Getting Started](#getting_started)
- [Usage](#usage)
- [Built Using](#built_using)
- [Contributing](../CONTRIBUTING.md)
- [Authors](#authors)

## 🧐 About <a name = "about"></a>

Even though there are many good journaling apps and tools I like to keep my setup simple. This script generates a Markdown file with today's date as name. 

## 🏁 Getting Started <a name = "getting_started"></a>

Just make sure you cover the [Prerequisites](#prerequisites) and [Installing](#installing) sections, then jump to [Usage](#usage) where I will demonstrate how to use the script.

### Prerequisites

As of now most of the Python modules used are part of Python's standard library. So no external dependencies should be installed.

### Installing

No special installation steps are needed, just clone this repo wherever you like:

```sh
git clone https://gitlab.com/thegrojas/jrnl-generator.git
cd jrnl-generator
make install
```

## 🎈 Usage <a name="usage"></a>

Let's assume that I want to keep my journal in a folder called `jrnl` in my home directory. Also I will stick to the basic template that comes with `jrnl-generator`. In this case I would run the following:

```sh
/opt/jrnl-generator/jrnl-generator --path ~/jrnl --template /opt/jrnl-generator/template.md
```

That's it! Now I should see the folders and file on `~/jrnl`.

### Cron

You don't want to run this everyday on your own. Use a cron job as the one bellow (this is the one that I use to generate my journal everyday at 0:00 AM):

```
0 0 * * * /opt/jrnl-generator/jrnl-generator --path /home/myuser/jrnl --template /opt/jrnl-generator/template.md
```

## ⛏️ Built Using <a name = "built_using"></a>
- [Python](https://www.python.org/) - Scripting Language

## ✍️ Authors <a name = "authors"></a>

- [@kylelobo](https://github.com/kylelobo) - Template for this README.

See also the list of [contributors]() who participated in this project.
