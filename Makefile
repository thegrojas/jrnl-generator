.PHONY: install uninstall

install:
	@echo "Installing jrnl-generator..."
	@mkdir -p -v /opt/jrnl-generator
	@cp ./jrnl-generator /opt/jrnl-generator/
	@cp ./template.md /opt/jrnl-generator/

uninstall:
	@echo "Uninstalling autojrnl..."
	@rm -r -f /opt/jrnl-generator
